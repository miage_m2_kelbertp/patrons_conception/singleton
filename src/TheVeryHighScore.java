public class TheVeryHighScore {

    private int privateScore;
    private static TheVeryHighScore instance;

    private TheVeryHighScore() {
        privateScore = 0;
    }

    public int getPrivateScore() {
        return privateScore;
    }

    public void setPrivateScore(int privateScore) {
        this.privateScore = privateScore;
    }

    public static TheVeryHighScore getInstance() {
        synchronized (TheVeryHighScore.class) {
            if (instance == null) {
                instance = new TheVeryHighScore();
            }
        }
        return instance;
    }


}
