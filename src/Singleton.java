public class Singleton {
    public static void main(String[] args) {
        TheVeryHighScore tvhs1 = TheVeryHighScore.getInstance();
        TheVeryHighScore tvhs2 = TheVeryHighScore.getInstance();

        System.out.println(tvhs1.getPrivateScore());

        tvhs1.setPrivateScore(5);

        System.out.println(tvhs2.getPrivateScore());

        tvhs2.setPrivateScore(10);

        System.out.println(tvhs1.getPrivateScore());

    }
}
